import numpy as np

def penalty(X, y, num = 200):
    """Generate a numpy array of penalty parameters :math:`\lambda` such that
    there's a good chance that the largest value in the array shrinks all
    coefficients in Lasso regression based on `X` and `y` to zero.

    Parameters
    ----------
    X: 2d np.array
        A 2 dimensional numpy array with :math:`N` observations in the rows and
        :math:`K` predictors.  This form is known in the statistics community as
        a model matrix.

    y: 1d np.array
        A 1 dimensional numpy array with :math:`N` observations in the rows.
        This is known as a response vector in the statistics community.

    Returns
    -------
    np.array
        Values of penalty parameter :math:`\lambda` to be used in some regularized linear method.

    """

    maxL = np.max(np.abs(np.matmul(np.transpose(X), y)))
    return np.logspace(-4, np.log10(maxL), num = num)
