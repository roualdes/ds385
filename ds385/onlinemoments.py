import numpy as np

class OnlineMoments:
    def __init__(self):
        self.m = 0
        self.v = 0
        self.N = 0

    def update(self, xs):
        if np.isscalar(xs):
            self._update(xs)
        elif np.ndim(xs) == 1:
            I = np.size(xs)
            for i in range(I):
                self._update(xs[i])
        else:
            raise ValueError("don't know what to do with dimensions > 1")

    def _update(self, x):
        self.N += 1
        w = 1 / self.N
        dm = x - self.m
        self.m += w * dm
        self.v += w * -self.v + w * (1 - w) * dm * dm

    def mean(self):
        return self.m

    def var(self, corrected=False):
        if corrected:
            return self.v * self.N / (self.N - 1)
        return self.v

    def reset(self):
        self.N = 0
        self.m = 0
        self.v = 0
