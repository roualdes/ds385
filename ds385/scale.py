import numpy as np
from .onlinemoments import OnlineMoments


def zscale(x, center=True, scale=True, corrected=False):
    om = OnlineMoments()
    om.update(x)
    y = x
    if center:
        y -= om.m
    if scale:
        y /= np.sqrt(om.v)
    return y
