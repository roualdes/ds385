Project
=======

Our course project will be a gentle introduction to a topic not directly covered
in class.  You will introduce the topic to your fellow classmates.  The topic
can be a subtopic of a topic we covered in class or something not covered in
class at all.

Since there are roughly 25 students in this class, and every student will give a
10 minute presentation, our projects will be due by the end of our scheduled
final exam minus 250 minutes of class time.

Deliverables
------------

You will

1. create a Jupyter notebook,
2. deliver a 10 minute presentation using 1., and
3. provide a compliment of every other students project


The Jupyter notebook is intended to help your classmates follow along your
introduction to your chosen project idea.  Your introduction should provide
context to the (sub)topic and provide at least two reasonable examples.

Your notebook and compliments will submit via GitHub in separate Jupyter
notebooks.  I'd like to automate (as much as possible) the distribution of
everyone's compliments, so there will be strict, and as of yet undetermined,
structure to follow for the compliments.


Project Ideas
-------------

Below is a list of project ideas and/or resources, including some ideas that
have already been taken (those with a 🚫).  The not taken project ideas are up
for grabs.  You may also use the list to help you think of other project ideas
not listed here.


If I have/find some good resources, I'll list them underneath each project idea.


* regex (computery)
   * 🚫 look ahead, look behind, capture groups
   * other subtopic ideas ...
   * `regex101 <https://regex101.com/>`_, be sure to select the Python flavor
   * `regexone <https://regexone.com/>`_

* 🚫 Python packages (computery)
   * Original tools `setuptools <https://setuptools.pypa.io/en/latest/>`_
   * Or newer tools `Alex Kyllo <https://alexkyllo.com/>`_'s post `Easy Python Package Publishing with Poetry <https://alexkyllo.com/posts/python-package/>`_
   * Python package `poetry <https://python-poetry.org/>`_
   * Don't forget documentation; use `Sphinx <https://www.sphinx-doc.org/en/master/>`_
   * Don't forget continuous integration.

* yada yada yada plots (statsy, computery)
   * `Tips for contour plots <https://jakevdp.github.io/PythonDataScienceHandbook/04.04-density-and-contour-plots.html>`_ in Python's Matplotlib from Jake Vanderplas.
   * 🚫 `Louis Tiao <http://louistiao.me/>`_ has a nice post about `visualizing and animating optimization algorithms with matplotlib <http://louistiao.me/notes/visualizing-and-animating-optimization-algorithms-with-matplotlib/>`_.
   * Making a plot to highlight some of points made in the following links would
     be great! `Sebastian Ruder <https://ruder.io/>`_ has a fairly popular blog
     post which provides an overview of some of the more popular gradient
     descent optimization algorithms, `An overview of gradient descent
     optimization algorithms <https://ruder.io/optimizing-gradient-descent/>`_.
     `John Chen <https://johnchenresearch.github.io/>`_ provided a follow up
     post `An updated overview of recent gradient descent algorithms
     <https://johnchenresearch.github.io/demon/>`_, which includes some more mordern methods.

* git (computery)
   * Read either `git book <https://git-scm.com/book/en/v2>`_ or `Getting Git Right <https://www.atlassian.com/git>`_, but stick to just one at first
   * 🚫 `Workflows
     <https://www.atlassian.com/git/tutorials/comparing-workflows>`_, stick to
     either Centralized or Feature Branch.
   * 🚫 `Oh Shit, Git!?! <https://ohshitgit.com/>`_ "Git is hard: screwing up is
     easy, and figuring out how to fix your mistakes is fucking impossible."

*  🚫 Does NBA draft pick order correlate with players' career success?

* Gradient Descent methods (mathy)
   * `Sebastian Ruder <https://ruder.io/>`_ has a fairly popular blog post which
     provides an overview of some of the more popular gradient descent
     optimization algorithms, `An overview of gradient descent optimization
     algorithms <https://ruder.io/optimizing-gradient-descent/>`_.
   * more resources to come...

* Linear Algebra on a computer (mathy, computery, advanced)
   * `Computational Linear Algebra for Coders
     <https://github.com/fastai/numerical-linear-algebra/blob/master/README.md>`_
   * `Introduction to Applied Linear Algebra – Vectors, Matrices, and Least
     Squares <https://web.stanford.edu/~boyd/vmls/>`_

* Deep Learning (mathy, computery, advanced)
   * `Practical Deep Learning for Coders <https://course.fast.ai/>`_

* Association Rules (statsy)
   * `Association Analysis: Basic Concepts and Algorithms
     <https://www-users.cse.umn.edu/~kumar001/dmbook/ch6.pdf>`_
   * `A Probabilistic Comparison of Commonly Used Interest Measures for
     Association Rules <https://mhahsler.github.io/arules/docs/measures>`_; pay
     attention to at least Support, Confidence, and Lift.

* 🚫 Dig deeper on the AITA subredit data.
