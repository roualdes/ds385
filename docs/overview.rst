Overview of ds385
=================

This website aims to help students get going in the wide wide world of data
science.  The functions within are meant to showcase simpler (and thus less
robust) implementations of more complex topics, which come up frequently
throughout data science.  Below you'll find some references that should be
useful at various times throughout the semester.


References
----------

Before we get to any Python, you should first familiarize yourself with at the
following topics from MIT's course `The Missing Semester of Your CS Education
<https://missing.csail.mit.edu/>`_:

* Course overview + the shell
* Shell Tools and Scripting
* Editors
* Data Wrangling
* Command-line Environment
* Version Control (Git)

We won't cover all the subtopics, but it wouldn't hurt you if you did.

If you don't have a working Python environment and IDE, then I suggest you
use `Jupyter Desktop <https://github.com/jupyterlab/jupyterlab-desktop>`_ for
DS385.  This isn't my recommendation for long term use of Python, but it's a
good start.

`Stanford's CS231n course <https://cs231n.github.io/>`_ has a good and
relatively quick `introduction to the Python programming language
<https://cs231n.github.io/python-numpy-tutorial/>`_.

.. TODO add https://regexone.com/

For some help getting started with building a Python package see `Alex Kyllo
<https://alexkyllo.com/>`_'s post `Easy Python Package Publishing with Poetry
<https://alexkyllo.com/posts/python-package/>`_. Of course the documentation for
the Python package `poetry <https://python-poetry.org/>`_, even if it takes a
while to figure out.

Documentation on the Sphinx markup language `reStructured Text
<https://sublime-and-sphinx-guide.readthedocs.io/en/latest/notes_warnings.html>`_.

`Tips for contour plots
<https://jakevdp.github.io/PythonDataScienceHandbook/04.04-density-and-contour-plots.html>`_
in Python's Matplotlib from Jake Vanderplas.

`Louis Tiao <http://louistiao.me/>`_ has a nice post about
`visualizing and animating optimization algorithms with matplotlib
<http://louistiao.me/notes/visualizing-and-animating-optimization-algorithms-with-matplotlib/>`_.

The book `Hands-On Machine Learning with R
<https://bradleyboehmke.github.io/HOML/>`_, by Bradley Boehmke & Brandon
Greenwellis, a great resource.

Drawing again on `Stanford's CS231n course <https://cs231n.github.io/>`_, I like
their discussion of `hyperparameter tuning
<https://cs231n.github.io/neural-networks-3/>`_.

`Sebastian Ruder <https://ruder.io/>`_ has a fairly popular blog post which
provides an overview of some of the more popular gradient descent optimization
algorithms, `An overview of gradient descent optimization algorithms
<https://ruder.io/optimizing-gradient-descent/>`_.  `John Chen
<https://johnchenresearch.github.io/>`_ provided a follow up post `An updated
overview of recent gradient descent algorithms
<https://johnchenresearch.github.io/demon/>`_, which includes some more mordern
methods.
