from ds385 import __version__
import ds385 as ds
import numpy as np
import pytest
from sklearn.linear_model import LinearRegression, Ridge, Lasso


def test_version():
    assert __version__ == "0.1.0"


class TestOnlineMoments:
    N = 100
    om = ds.OnlineMoments()
    x = np.random.normal(size=N)

    m = np.mean(x)
    v = np.var(x)

    def test_update_iteratively(self):
        for n in range(self.N):
            self.om.update(self.x[n])

        assert np.isclose(self.om.m, self.m)
        assert np.isclose(self.om.v, self.v)

    def test_reset(self):
        self.om.reset()
        assert np.isclose(self.om.m, 0)
        assert np.isclose(self.om.v, 0)
        assert np.isclose(self.om.N, 0)

    def test_update_bulk(self):
        self.om.update(self.x)
        assert np.isclose(self.om.m, self.m)
        assert np.isclose(self.om.v, self.v)


class TestScale:
    def test_center(self):
        x = np.random.normal(loc=-10, scale=5, size=100)
        z = ds.zscale(x, scale=False)
        assert np.isclose(np.mean(z), 0)
        assert not np.isclose(np.var(z), 1)

    def test_scale(self):
        x = np.random.normal(loc=-10, scale=5, size=100)
        z = ds.zscale(x, center=False)
        assert np.isclose(np.var(z), 1)
        assert not np.isclose(np.mean(z), 0)

    def test_center_and_scale(self):
        x = np.random.normal(loc=-10, scale=5, size=100)
        z = ds.zscale(x)
        assert np.isclose(np.mean(z), 0)
        assert np.isclose(np.var(z), 1)


class TestCoordinateDescent:
    N = 100
    K = 3
    X = np.c_[np.ones(N), np.random.normal(size=(N, K))]
    y = np.matmul(X, np.array([1, 2, 3, 4])) + np.random.normal(size=N)

    def test_LinearRegression(self):
        fit = LinearRegression().fit(self.X[:, [1, 2, 3]], self.y)
        beta = ds.cd(self.X, self.y)
        assert np.allclose(beta[1 : self.K + 1], fit.coef_, atol=1e-5)
        assert np.allclose(beta[0], fit.intercept_, atol=1e-5)

    def test_Lasso(self):
        # NB the penalty parameter is inconsistent, because the loss function is
        # different, between Lasso and Ridge
        fit = Lasso(alpha=1 / self.N).fit(self.X[:, [1, 2, 3]], self.y)
        beta = ds.cd(self.X, self.y, l=1, l1=True)
        assert np.allclose(beta[1 : self.K + 1], fit.coef_, atol=1e-5)
        assert np.allclose(beta[0], fit.intercept_, atol=1e-5)

    def test_Ridge(self):
        fit = Ridge(alpha=1).fit(self.X[:, [1, 2, 3]], self.y)
        beta = ds.cd(self.X, self.y, l=1, l2=True)
        assert np.allclose(beta[1 : self.K + 1], fit.coef_, atol=1e-5)
        assert np.allclose(beta[0], fit.intercept_, atol=1e-5)
