.. ds385 documentation master file, created by
   sphinx-quickstart on Fri May 20 07:58:58 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ds385's documentation!
=================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   overview
   notebooks
   reference
   project
   notebooks/activities

.. bibliography:: refs.bib

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
