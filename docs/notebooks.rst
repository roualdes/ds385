Notebooks
=========

The pages below form a rough outline of the topics of the course and contain the
basic structure of many of our lectures.

.. toctree::
   :caption: Notebooks:
   :maxdepth: 1


   notebooks/text_data
   notebooks/regex
   notebooks/basic_plots
   notebooks/activities
   notebooks/aggregate_groupby
   notebooks/distributions
   notebooks/one_d_optimization
   notebooks/online_mean
   notebooks/3d_plots
   notebooks/two_d_optimization
   notebooks/linear_regression
   notebooks/group_means
   notebooks/multiple_linear_regression
   notebooks/logistic_regression
   notebooks/regularized_multiple_linear_regression
   notebooks/regularized_logistic_regression
   notebooks/intro_kfold_crossvalidation
..
    re-visit regularized_logistic_regression
    stochastic gradient descent
    momentum
